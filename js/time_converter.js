(function($) {
    Drupal.behaviors.timeConverterFormatter = {
        attach: function(context, settings) {
           $(document).ready(function(){


               $('.time-converter').on('click', function() {
                   $(this).html('Converting');
                   setTimeout(function(){
                       var usertimezone = moment.tz.guess();
                       var timezone = Drupal.settings.time_converter.timezone;
                       var startdate  = moment.tz(Drupal.settings.time_converter.startdatefull, timezone);
                       var enddate    = moment.tz(Drupal.settings.time_converter.enddatefull, timezone);
                       var startdayorigin = Drupal.settings.time_converter.startdayorigin;
                       var startmonthorigin = Drupal.settings.time_converter.startmonthorigin;
                       var enddayorigin = Drupal.settings.time_converter.enddayorigin;
                       var endmonthorigin = Drupal.settings.time_converter.endmonthorigin;
                       var startdatelocale = startdate.clone().tz(usertimezone).format("HH:mm");
                       var startdaylocale = startdate.clone().tz(usertimezone).format("DD");
                       var startmonthlocale = startdate.clone().tz(usertimezone).format("MMMM");
                       var enddatelocale = enddate.clone().tz(usertimezone).format("HH:mm zZ");
                       var enddaylocale = enddate.clone().tz(usertimezone).format("DD");
                       var endmonthlocale = enddate.clone().tz(usertimezone).format("MMMM");
                       var time = startdatelocale + ' to ' + enddatelocale;
                       if((startdaylocale == enddaylocale) && (startmonthlocale == endmonthlocale)){
                           if((startdaylocale != startdayorigin) && (startmonthorigin != startmonthlocale)&& (endmonthorigin != endmonthlocale)){
                               var dates = startdaylocale + ' ' + startmonthlocale;
                               var time = startdatelocale + ' to ' + enddatelocale;
                               $('.time-container').html(time);
                               $('.timezone-container').html(dates);
                           }
                           var time = startdatelocale + ' to ' + enddatelocale;
                           $('.time-container').html(time);
                       } else{
                           var dates = startdaylocale + ' ' + startmonthlocale + ' ' + startdatelocale  + ' to ' + enddaylocale + ' ' + endmonthlocale + ' ' + enddatelocale;
                           $('.time-container').hide();
                           $('.timezone-container').html(dates);
                       }

                       $('.time-converter').hide();

                   }, 2000);
               });
           });
        }
    };
}(jQuery));
